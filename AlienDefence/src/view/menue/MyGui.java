package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;

public class MyGui extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGui frame = new MyGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 560);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAufgabe1 = new JLabel(" Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabe1.setBounds(10, 95, 198, 28);
		contentPane.add(lblAufgabe1);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRot_clicked();
			}
		});
		btnRot.setBounds(10, 134, 89, 23);
		contentPane.add(btnRot);
		
		JButton btnGr�n = new JButton("Gr\u00FCn");
		btnGr�n.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGr�n_clicked();
			}
		});
		btnGr�n.setBounds(109, 134, 89, 23);
		contentPane.add(btnGr�n);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlau_clicked();
			}
		});
		btnBlau.setBounds(208, 134, 89, 23);
		contentPane.add(btnBlau);
		
		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(10, 181, 188, 14);
		contentPane.add(lblAufgabe2);
		
		JLabel lblTEXT = new JLabel("Dieser Text soll ver\u00E4ndert werden");
		lblTEXT.setBounds(10, 11, 414, 71);
		contentPane.add(lblTEXT);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonArial_clicked();
			}
		});
		btnArial.setBounds(10, 206, 89, 23);
		contentPane.add(btnArial);
	}


	public void buttonArial_clicked() {
		lblTEXT.setFont(new Font("Arial", Font.PLAIN, 12));
		
	}

	public void buttonBlau_clicked() {
		this.contentPane.setBackground(Color.BLUE);
		
	}

	public void buttonGr�n_clicked() {
		this.contentPane.setBackground(Color.GREEN);
		
	}

	public void buttonRot_clicked() {
		this.contentPane.setBackground(Color.RED);
		
	}
}
